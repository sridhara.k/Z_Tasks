# Z_Task
Z_Ops_Tasks

1- Create a `Dockerfile` for the app.

2- Create a `docker-compose.yml` for the app which includes

  - `redis` service, with the data directory of `redis` mounted at `/data` in your VM.
  - `app` service running with port `8000` exposed to the host.

3- Write a bash script that creates and boots [Vagrant box](https://vagrant.io) with Ubuntu.

4- Using Ansible provision the VM to:

  - Setup hostname of VM as `demo-ops`
  - Create a user `demo`
  - Configure `sysctl` for sane defaults. For eg: increasing open files limit. Configure a variety of `sysctl` settings to make the VM a production grade one.
  - Set the system's timezone to "Asia/Kolkata"
  - Install Docker and Docker-Compose
  - Configure Docker Daemon to have sane defaults. For eg: to keep logs size in check.
  - Deploy the `docker-compose.yml` in `/etc/demo-ops` and start the services

### Bonus Section

Task 5
- Create a namespace `demo-ops`
- Create a deployment and service manifest for the app.
- Configure liveliness check, resource quotas for the deployment.

